#!/usr/bin/env python3
import tkinter as Tk
import mainwindow

root = Tk.Tk()
root.minsize(width=700, height=400)
app = mainwindow.MainWindow()
root.mainloop()