class Selector(object):
	"""Selector for Gopher documents"""
	def __init__(self, hostname, path='', port=70):
		super(Selector, self).__init__()

		assert type(port) is int
		assert port > 0
		assert len(hostname) > 0

		self.hostname = hostname
		self.port = port
		self.path = path

	def __str__(self):
		return '%s:%s%s' % (self.hostname, self.port, self.path)