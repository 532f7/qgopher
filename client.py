from selector import Selector
from entry import Entry
import socket

class Client(object):
	"""docstring for Client"""
	def __init__(self):
		super(Client, self).__init__()

	def fetch(self, selector):
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect((selector.hostname, selector.port))
		sock.sendall((selector.path + '\n').encode())

		buf = []
		resp = sock.recv(4096)
		while len(resp) != 0:
			buf.append(resp)
			resp = sock.recv(4096)

		# not sure if there's a better way to do this than concatenating
		# everything -- we'd have to stream the buffer in and look for \n's
		lines = ''.join([x.decode() for x in buf]).split('\n')

		entries = [Entry(selector, line) for line in lines if len(line) > 0]
		return entries

	def fetch_text(self, selector):
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect((selector.hostname, selector.port))
		sock.sendall((selector.path + '\n').encode())

		buf = []
		resp = sock.recv(4096)
		while len(resp) != 0:
			buf.append(resp)
			resp = sock.recv(4096)

		text = ''.join([x.decode() for x in buf])
		return text