import tkinter as tk
from tkinter import BOTH, LEFT, N, E, S, W
import tkinter.ttk as ttk

from selector import Selector

class MainWindow(ttk.Frame):
	def __init__(self):
		super(MainWindow, self).__init__()

		self.master.title("qGopher")
		self.pack(fill=BOTH, expand=1)

		self.init_navbar()
		self.init_listview()
		self.init_statusbar()
		
		self.set_status("gGopher 0.001")

	def init_navbar(self):
		self.columnconfigure(0, weight=1)
		nav_frame = ttk.Frame(self, relief=tk.RAISED, borderwidth=1)
		nav_frame.columnconfigure(1, weight=1)
		nav_frame.grid(row=0, column=0, sticky=W+E)

		lbl_path = ttk.Label(nav_frame, text="Path")
		lbl_path.grid(row=0, column=0)

		entry_path = ttk.Entry(nav_frame)
		entry_path.insert(0, 'gopher.quux.org/')
		entry_path.grid(row=0, column=1, sticky=W+E, padx=10)
		self.entry_path = entry_path

		lbl_port = ttk.Label(nav_frame, text="Port")
		lbl_port.grid(row=0, column=2)

		# set up validation to limit values to numbers
		vcmd = self.register(self.validate_numeric)
		entry_port = ttk.Entry(nav_frame, width=4, validate='all',
			validatecommand=(vcmd, '%P'))
		entry_port.insert(0, '70')
		entry_port.grid(row=0, column=3, sticky=W+E, padx=10)
		self.entry_port = entry_port

		button_go = ttk.Button(nav_frame, text='Go', command=self.navigate)
		button_go.grid(row=0, column=4)

		# bind enter key
		self.master.bind("<Return>", self.navigate)

	def init_listview(self):
		self.rowconfigure(1, weight=1)
		#todo: create real list view
		view = ttk.Frame(self, relief=tk.SUNKEN, borderwidth=1)
		view.grid(row=1, column=0, sticky=N+W+S+E)

	def init_statusbar(self):
		self.status = tk.StringVar()
		lbl_status = ttk.Label(self, textvariable=self.status)
		lbl_status.grid(row=2,sticky=E+W+S)
		self.lbl_status = lbl_status
		self.lbl_status_default_bg = lbl_status.cget("background")

	def set_status(self, msg, error=False):
		self.status.set(msg)

		if error:
			self.lbl_status.configure(background='#faa')
		else:
			self.lbl_status.configure(background=self.lbl_status_default_bg)

	def get_selector(self):
		port = self.entry_port.get()
		if not str.isnumeric(port):
			raise ValueError("invalid port")
		port = int(port)
		if not port > 0:
			raise ValueError("invalid port")

		path = self.entry_path.get()
		separator_idx = path.find('/')
		if separator_idx == -1:
			host = path
			resource = '/'
		else:
			host = path[:separator_idx]
			resource = path[separator_idx:]
		return Selector(host, resource, port)

	def navigate(self, event=None):
		try:
			selector = self.get_selector()
			self.set_status("get " + str(selector))
		except ValueError as e:
			self.set_status("ERROR: " + str(e), True)

	def validate_numeric(self, P):
		return str.isnumeric(P) or P == ''