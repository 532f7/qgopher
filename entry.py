from selector import Selector

TYPE_INFO = 'i'
TYPE_DIRECTORY = '1'
TYPE_TEXT = '0'
TYPE_BINARY = '9'
TYPE_ERROR = '3'
TYPE_HTML = 'h'
ALL_TYPES = [
	TYPE_INFO,
	TYPE_TEXT,
	TYPE_DIRECTORY,
	TYPE_BINARY,
	TYPE_ERROR,
	TYPE_HTML,
]

class Entry(object):
	"""Represents a menu entry"""
	def __init__(self, selector, line):
		super(Entry, self).__init__()

		self.selector = selector

		sections = line.split('\t')

		if len(sections) < 4:
			raise ValueError('< 4 sections in line', line)

		if len(sections[0]) == 0:
			raise ValueError("Can't find a message type for section (empty)", line)

		gopher_type = sections[0][0]
		if gopher_type not in ALL_TYPES:
			raise ValueError("Invalid message type for line", line)
		self.gopher_type = gopher_type

		if len(sections[0]) > 1:
			self.label = sections[0][1:]
		else:
			self.label = ""

		if gopher_type is TYPE_INFO:
			self.link = None
		else:
			path = sections[1]
			host = sections[2]
			port = int(sections[3])
			self.link = Selector(host, path, port)

	def __str__(self):
		if self.gopher_type is TYPE_INFO:
			return '%s: %s' % (self.gopher_type, self.label)
		else:
			return '%s: %s %s' % (self.gopher_type, self.label, self.link)